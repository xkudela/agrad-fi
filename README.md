# Agrad (Agriculture advertisements)

Tato webová aplikace umožňuje uživatelům inzerovat zemědělské produkty. Prototyp funkčního řešení se nachází na [univerzitním serveru](https://crocs-monitor.ics.muni.cz)

## Použité technologie

- **Backend:** Java Spring Boot
- **Frontend:** React

## Instalace a spuštění

### Backend (Java Spring Boot)

1. **Instalace databáze PostgreSQL:**
    - Nainstalujte PostgreSQL a rozšíření PostGIS.
    - Vytvořte novou databázi.

2. **Nastavení application.properties:**
    - Nastavte připojení k databázi a další konfigurace v souboru `application.properties`.
    - Přidejte certifikát pro HTTPS připojení, pokud je potřeba.

3. **Spouštění projektu:**
    - Nainstalujte Java Development Kit (JDK) verze 17 nebo vyšší.
    - Spusťte projekt pomocí Maven příkazu `mvn spring-boot:run`.

### Frontend (React)

1. **Instalace Node.js a npm:**
    - Nainstalujte Node.js a npm.

2. **Nastavení konfigurace:**
    - Nastavte připojení k Auth0, k Mapy.cz API a k backendu v souboru `Config.js`.

3. **Instalace závislostí:**
    - Přejděte do adresáře `frontend` ve zdrojovém kódu aplikace.
    - Nainstalujte závislosti pomocí příkazu `npm install`.

4. **Spouštění aplikace:**
    - Spusťte aplikaci pomocí příkazu `npm start`.

## API

Aplikace poskytuje následující rozhraní:

### `/api/ad`

- Endpointy pro práci s inzeráty pro všechny uživatele.

### `/api/adAuth`

- Endpointy pro práci s inzeráty pro přihlášené uživatele.

### `/api/register`

- Endpoint pro registraci nového uživatele do systému.

### `/api/user`

- Endpointy pro práci s uživatelským účtem pro přihlášené uživatele.

Pro podrobnější přehled a dokumentaci API
navštivte: [Swagger UI](https://crocs-monitor.ics.muni.cz:8443/swagger-ui/index.html)

## Autor

- **Jméno:** Richard Kuděla

## Licence

Z důvodu použití komerční komponenty TailwindUI byl kód z této komponenty nahrazen komentářem.

Tento projekt je licencován pod MIT License. Pro více informací si přečtěte [LICENSE](LICENSE).
