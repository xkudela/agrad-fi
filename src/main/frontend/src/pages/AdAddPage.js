import React, {Fragment, useRef, useState} from 'react';
import {useTranslation} from "react-i18next";
import {API_BASE_URL_WITH_PORT} from '../Config';
import {useAdTypes} from "../contexts/CategoriesContext";
import {useNavigate} from "react-router-dom";
import {StyledDropzone} from "../components/StyledDropzone";
import UserInfo from "../components/UserInfo";
import {useAuth} from "../contexts/AccessTokenContext";

const AddAdPage = () => {
    // state definitions
    const [adData, setAdData] = useState({
        id: '',
        name: '',
        price: '',
        description: '',
        transactionCategory: 'OFFER',
    });

    const [selectedCategory, setSelectedCategory] = useState('');
    const [selectedImages, setSelectedImages] = useState([]);
    const [address, setAddress] = useState({});
    const [optionalFields, setOptionalFields] = useState([]);
    const [additionalFields, setAdditionalFields] = useState({});
    const [open, setOpen] = useState(false);
    const [responseId, setResponseId] = useState('');
    const cancelButtonRef = useRef(null);

    // context and hooks
    const {accessToken} = useAuth();
    const navigate = useNavigate();
    const {t} = useTranslation();
    const adTypes = useAdTypes();

    const handleChange = (e) => {
        const {name, value} = e.target;

        setAdData((prevData) => ({
            ...prevData,
            [name]: value,
        }));
        console.log(adData);
    };

    const handleAdditionalFieldChange = (e) => {
        const {name, value} = e.target;

        setAdditionalFields((prevFields) => ({
            ...prevFields,
            [name]: value,
        }));
    };

    const handleCategoryChange = async (e) => {
        const selectedCategory = e.target.value;
        setSelectedCategory(selectedCategory);
        if (selectedCategory) {
            try {
                const response = await fetch(`${API_BASE_URL_WITH_PORT}/api/ad/categories/${selectedCategory}`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${accessToken}`,
                    },
                });
                if (response.ok) {
                    const data = await response.json();
                    const fields = Object.entries(data).map(([, fieldValue]) => {
                        return (
                            <div key={fieldValue} className="w-full md:w-1/2 lg:w-1/3 px-2 mb-4">
                                <label htmlFor={fieldValue} className="block text-sm font-medium text-gray-700">
                                    {t(fieldValue.charAt(0).toUpperCase() + fieldValue.slice(1))}
                                </label>
                                <input
                                    type="text"
                                    name={fieldValue}
                                    value={additionalFields.fieldValue}
                                    onChange={handleAdditionalFieldChange}
                                    className="mt-1 p-2 border rounded-md w-full"
                                />
                            </div>
                        );
                    });
                    setOptionalFields(fields);
                } else {
                    console.error('Failed to fetch category data:');
                    // Handle error
                }
            } catch (error) {
                console.error('Error fetching category data:', error);
                // Handle error
            }
        } else {
            setOptionalFields([]);
        }
    };


    const handleSubmit = async (e) => {
        e.preventDefault();

        const formData = new FormData();
        selectedImages.forEach(file => formData.append('files', file));
        // first upload image
        try {
            const uploadResponse = await fetch(`${API_BASE_URL_WITH_PORT}/api/adAuth/upload`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${accessToken}`,
                },
                body: formData,
            });

            if (!uploadResponse.ok) {
                throw new Error('Failed to upload images');
            }

            const adIdResponse = await uploadResponse.json();

            // prepare rest with received image id
            const adPayload = {
                ...Object.entries(adData).reduce((acc, [key, value]) => {
                    if (value !== '' && value !== null && value !== undefined) {
                        acc[key] = value;
                    }
                    return acc;
                }, {}),
                category: selectedCategory,
                address: address,
                adId: adIdResponse,
                additionalFields: {},
            };

            Object.entries(additionalFields).forEach(([name, value]) => {
                adPayload.additionalFields[name] = value;
            });

            const adResponse = await fetch(`${API_BASE_URL_WITH_PORT}/api/adAuth`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${accessToken}`,
                },
                body: JSON.stringify(adPayload),
            });

            if (!adResponse.ok) {
                throw new Error('Failed to submit ad');
            }
            setOpen(true);
            setResponseId(adIdResponse);
        } catch (error) {
            console.error('Error during ad submission:', error.message);
        }
    };

    const handleNavigateClick = () => {
        setOpen(false);
        navigate(`/ad/${responseId}`);
    }


    // dynamic form rendering
    const renderAdditionalFields = () => {

        // handle description separately
        const descriptionField = (
            <div className="w-full px-2 mb-4">
                <label htmlFor="description" className="block text-sm font-medium text-gray-700">
                    {t("Description")}:
                </label>
                <textarea
                    name="description"
                    value={adData.description}
                    onChange={handleChange}
                    className="mt-1 p-2 border rounded-md w-full"
                    required
                />
            </div>
        );

        const requiredFields = ['title', 'price', 'offerDemand'].map((field) => (
            <div key={field} className="w-full md:w-1/2 lg:w-1/3 px-2 mb-4">
                {field === 'offerDemand' ? (
                    <div>
                        <label className="block text-sm font-medium text-gray-700">{t("Offer/Demand")}:</label>
                        <select
                            name="offerDemand"
                            value={adData.offerDemand}
                            onChange={handleChange}
                            className="mt-1 p-2 border rounded-md w-full"
                            required
                        >
                            <option value="offer">{t("Offer")}</option>
                            <option value="demand">{t("Demand")}</option>
                        </select>
                    </div>
                ) : (
                    <div>
                        <label htmlFor={field} className="block text-sm font-medium text-gray-700">
                            {t(field.charAt(0).toUpperCase() + field.slice(1))}
                        </label>
                        <input
                            type={field !== 'price' ? 'text' : 'number'}
                            name={field}
                            value={adData[field]}
                            onChange={handleChange}
                            className="mt-1 p-2 border rounded-md w-full"
                            required
                        />
                    </div>
                )}
            </div>
        ));

        return (
            <div className="flex flex-wrap">
                {requiredFields}
                {optionalFields}
                {descriptionField}
                <StyledDropzone selectedImages={selectedImages} setSelectedImages={setSelectedImages}/>
            </div>
        );
    };

    return (
        <>
            {/*Tailwind UI komponenta*/}

            <div>
                <div>
                    <UserInfo address={address} setAddress={setAddress}/>
                    {/*Tailwind UI komponenta*/}
                </div>
            </div>
        </>
    );

};

export default AddAdPage;
