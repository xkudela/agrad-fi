export const API_BASE_URL = "<FRONTEND_URL>";
export const PORT = "<BACKEND_PORT>";
export const API_BASE_URL_WITH_PORT = `${API_BASE_URL}:${PORT}`;
export const AUTH0_DOMAIN = "<AUTH_DOMAIN>";
export const AUTH0_CLIENT_ID = "<AUTH_CLIENT_ID>";
export const AUDIENCE = `${API_BASE_URL}/api/auth0`;
export const API_MAPY_CZ_KEY = "<MAPY_CZ_API_KEY>";