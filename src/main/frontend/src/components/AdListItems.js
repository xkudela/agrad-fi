import {useTranslation} from "react-i18next";
import {API_BASE_URL_WITH_PORT} from "../Config";
import {useAuth} from "../contexts/AccessTokenContext";
import React, {useEffect, useState} from "react";
import LoadingSpinner from "./LoadingSpinner";

const AdListItems = ({ads, ownAds}) => {
        const {t} = useTranslation();
        const {accessToken} = useAuth();
        const [isDeleteLoading, setIsDeleteLoading] = useState(false);

        useEffect(() => {
        }, [isDeleteLoading]);

        const deleteAd = async (adId) => {
            try {
                if (accessToken !== null) {
                    setIsDeleteLoading(true);
                    await fetch(`${API_BASE_URL_WITH_PORT}/api/adAuth/${adId}`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${accessToken}`,
                        },
                    });
                }
            } catch (error) {
                console.error(error);
            }
        }


        return (
            isDeleteLoading ? (
                    <LoadingSpinner/>
                ) :
                null // Tailwind UI komponenta)
        );
    }
;

export default AdListItems;