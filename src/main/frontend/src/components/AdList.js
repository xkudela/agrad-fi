import React, {useEffect, useState} from 'react';
import {API_BASE_URL_WITH_PORT} from '../Config';
import {useTranslation} from "react-i18next";
import LoadingSpinner from "./LoadingSpinner";

const AdList = ({keyword, filter}) => {
    const [ads, setAds] = useState([]);
    const [noAds, setNoAds] = useState(false);
    const [loading, setLoading] = useState(true);
    const {t} = useTranslation();

    useEffect(() => {
        const fetchAds = async () => {
            try {
                const requestFilter = {
                    ...filter, address: filter.address?.data
                };

                const response = await fetch(`${API_BASE_URL_WITH_PORT}/api/ad/all`, {
                    method: 'POST',
                    headers: {'Content-Type': 'application/json'},
                    body: JSON.stringify(requestFilter)
                });

                if (response.ok) {
                    const data = await response.json();
                    setAds(data);
                    setNoAds(data.length === 0);
                } else {
                    console.error('Failed to fetch ads');
                }
            } catch (error) {
                console.error('Error while fetching ads:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchAds();
    }, [keyword, filter]);

    const handleClick = (adId) => {
        fetch(`${API_BASE_URL_WITH_PORT}/api/ad/increaseViews/${adId}`)
            .then(response => {
                if (!response.ok) {
                    console.error('Failed to increase view count');
                }
            })
            .catch(error => {
                console.error('Error increasing view count:', error);
            });
    }

    return (
        loading ? <LoadingSpinner/> :
            null // Tailwind UI komponenta
    );
};

export default AdList;
