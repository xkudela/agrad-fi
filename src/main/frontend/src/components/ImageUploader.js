import React, {useState} from 'react';
import 'react-image-gallery/styles/css/image-gallery.css';
import ImageGallery from 'react-image-gallery';

function ImageUploader({setSelectedImages}) {
    const [images, setImages] = useState([]);

    const handleImageChange = (e) => {
        const files = Array.from(e.target.files);
        setImages([...images, ...files]);
        setSelectedImages([...images, ...files]);
    };

    const handleRemoveImage = (index) => {
        const newImages = [...images];
        newImages.splice(index, 1);
        setImages(newImages);
        setSelectedImages(newImages);
    };

    const imageObjects = images.map((image, index) => ({
        original: URL.createObjectURL(image),
        thumbnail: URL.createObjectURL(image),
        description: `Image ${index}`,
        renderItem: () => (
            <div className="relative">
                <img src={URL.createObjectURL(image)} alt={`Image ${index}`}/>
                <button className="absolute top-2 right-2 bg-red-500 text-white rounded-full px-2 py-1"
                        onClick={() => handleRemoveImage(index)}>Remove
                </button>
            </div>
        ),
    }));

    const handleAddImageClick = () => {
        document.getElementById('fileInput').click();
    };

    return (
        <div>
            <div className="relative mb-8">
                <input id="fileInput" type="file" multiple onChange={handleImageChange} className="hidden"/>
                <button className="absolute top-2 right-2 bg-blue-500 text-white rounded px-4 py-2"
                        onClick={handleAddImageClick}>Přidat obrázek
                </button>
            </div>
            <ImageGallery items={imageObjects}/>
        </div>
    );
}

export default ImageUploader;
