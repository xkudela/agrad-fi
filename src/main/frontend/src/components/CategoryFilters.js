import React, {useState} from 'react'
import {useAdTypes} from "../contexts/CategoriesContext";
import Filters from "./Filters";

export default function CategoryFilters({setFilter}) {
    const [mobileFiltersOpen, setMobileFiltersOpen] = useState(false);
    const [ownFilter, setOwnFilter] = useState({});
    const adTypes = useAdTypes();


    const handleFiltersChange = () => {
        setFilter(ownFilter);
        if (mobileFiltersOpen) {
            setMobileFiltersOpen(false);
        }
    }

    const resetFilter = () => {
        setOwnFilter({});
        setFilter({});
    }

    const handleAdTypeToggle = (category) => {
        const updatedFilter = {...ownFilter};
        updatedFilter.category = '';
        updatedFilter.adType = ownFilter.adType === category ? '' : category;
        setOwnFilter(updatedFilter);
    };

    const handleAdTypeHoverOut = () => {
        if (!ownFilter.adType) {
            setOwnFilter({...ownFilter, adType: ''});
        }
    };

    const handleCategoryClick = (category) => {
        setOwnFilter({...ownFilter, category});
    };

    const handleKeywordChange = (e) => {
        setOwnFilter({...ownFilter, keyword: e.target.value})
    };

    return (
        <div className="bg-white">
            <div>
                {/*Tailwind UI komponenta*/}
                <Filters mobile={true} filter={ownFilter} setFilter={setOwnFilter}/>
            </div>
        </div>
    )
}