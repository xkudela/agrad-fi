import {useEffect, useState} from 'react';
import {API_BASE_URL_WITH_PORT, API_MAPY_CZ_KEY} from '../Config';
import {useTranslation} from "react-i18next";
import Utils from "../Utils";

function Filters({mobile, filter, setFilter}) {
    const [minPriceConst, setMinPriceConst] = useState(0);
    const [maxPriceConst, setMaxPriceConst] = useState(0);
    const [regions, setRegions] = useState([]);
    const {t} = useTranslation();
    const [loading, setLoading] = useState(false);
    const [address, setAddress] = useState({});
    const [keyword, setKeyword] = useState('');

    useEffect(() => {
        fetchFilters();
    }, []);


    const fetchFilters = async () => {
        try {
            const response = await fetch(`${API_BASE_URL_WITH_PORT}/api/ad/filters`);
            if (response.ok) {
                const data = await response.json();
                setRegions(data.regions);
                setMinPriceConst(data.minPrice);
                setMaxPriceConst(data.maxPrice);
            } else {
                console.error('Failed to fetch filters:', response.statusText);
            }
        } catch (error) {
            console.error('Error while fetching filters:', error);
        }
    };


    const handleMaxPriceChange = (event) => {
        setFilter({...filter, maxPrice: parseInt(event.target.value)})
    };

    const handleMinPriceChange = (event) => {
        setFilter({...filter, minPrice: parseInt(event.target.value)})
    };

    const handleRadiusChange = (event) => {
        setFilter({...filter, radius: parseInt(event.target.value)})
    };

    const handleAddressChange = (selectedOption) => {
        setFilter({...filter, address: selectedOption})
    };


    const handleRegionChange = (regionKey) => {
        const isSelected = filter.regions?.includes(regionKey);
        let updatedRegions;
        if (isSelected) {
            updatedRegions = filter.regions?.filter(region => region !== regionKey);
        } else {
            updatedRegions = [...(filter.regions || []), regionKey];
        }
        setFilter({...filter, regions: updatedRegions});
    };

    const fetchOptions = async (inputValue) => {
        const options = Utils.fetchMapyCzOptions(inputValue, API_MAPY_CZ_KEY);
        setLoading(false);
        return options;
    };


    return (
        mobile ? null // Tailwind UI komponenta
            : null // Tailwind UI komponenta
    );
}

export default Filters;
