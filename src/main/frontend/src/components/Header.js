import {useTranslation} from "react-i18next";
import {useEffect, useState} from "react";
import {useAuth} from "../contexts/AccessTokenContext";
import {API_BASE_URL_WITH_PORT} from "../Config";

const Header = ({triggerToggle}) => {
    const {isAuthenticated, loginWithRedirect, logout, accessToken, user} = useAuth();
    const {t} = useTranslation();
    const [favoriteAds, setFavoriteAds] = useState([]);

    const userNavigation = isAuthenticated
        ? [
            {name: t("My profile"), to: '/profile'},
            {name: t("My advertisements"), to: '/my-ads'},
            {name: t("Logout"), handleClick: logout},
        ]
        : [
            {name: t("Add advertisement"), to: '/benefits'},
            {name: t("Login"), handleClick: loginWithRedirect},
        ];

    function classNames(...classes) {
        return classes.filter(Boolean).join(' ')
    }

    function getInitials(text) {
        const words = text.split(" ");
        const firstInitial = words[0] ? words[0][0] : '';
        const secondInitial = words[1] ? words[1][0] : '';
        return `${firstInitial}${secondInitial}`;
    }

    useEffect(() => {
        const fetchFavoriteAds = async () => {
            try {
                if (accessToken !== null) {
                    const response = await fetch(`${API_BASE_URL_WITH_PORT}/api/adAuth/favoriteAds`, {
                        headers: {
                            Authorization: `Bearer ${accessToken}`,
                        },
                    });
                    setFavoriteAds(await response.json());
                }
            } catch (error) {
                console.error(error);
            }
        };

        fetchFavoriteAds();
    }, [accessToken, triggerToggle]);

    return (
        null // Tailwind UI komponenta
    );
}

export default Header;