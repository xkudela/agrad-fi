package com.agrad.backend.model;

/**
 * @author richard.kudela
 * @since 25.11.2023
 */

import com.agrad.backend.model.enums.TransactionCategory;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Advertisement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private Double price;

    @Lob
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "address_id")
    private Address address;

    @Enumerated(EnumType.STRING)
    private TransactionCategory transactionType;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private AdvertisementCategory category;

    @OneToMany(mappedBy = "advertisement", cascade = CascadeType.ALL)
    private List<AdData> adDataList = new ArrayList<>();

    private String imagePath;

    private LocalDateTime creationDate;

    private int viewCount;
}

