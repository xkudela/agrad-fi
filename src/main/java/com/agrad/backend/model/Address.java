package com.agrad.backend.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;

/**
 * @author richard.kudela
 * @since 25.11.2023
 */
@Entity
@Data
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String location;

    private String region;

    @Column(columnDefinition = "Geometry(Point, 4326)")
    private Point coordinates;

    public Address() {
    }

    public Address(String name, String location, String region, Double longitude, Double latitude) {
        this.name = name;
        this.location = location;
        this.region = region;
        this.coordinates = createPoint(longitude, latitude);
    }

    private Point createPoint(Double longitude, Double latitude) {
        GeometryFactory geometryFactory = new GeometryFactory();
        Coordinate coordinate = new Coordinate(longitude, latitude);
        return geometryFactory.createPoint(coordinate);
    }

    public Double getLongitude() {
        return coordinates != null ? coordinates.getX() : null;
    }

    public void setLongitude(Double longitude) {
        if (coordinates == null) {
            coordinates = createPoint(longitude, getLatitude());
        } else {
            coordinates.getCoordinate().setX(longitude);
        }
    }

    public Double getLatitude() {
        return coordinates != null ? coordinates.getY() : 0D;
    }

    public void setLatitude(Double latitude) {
        if (coordinates == null) {
            coordinates = createPoint(getLongitude(), latitude);
        } else {
            coordinates.getCoordinate().setY(latitude);
        }
    }

}
