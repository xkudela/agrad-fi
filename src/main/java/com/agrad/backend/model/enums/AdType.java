package com.agrad.backend.model.enums;

import lombok.Getter;

@Getter
public enum AdType {
    MACHINE,
    ANIMAL,
    COMMODITY,
    SERVICE
}
