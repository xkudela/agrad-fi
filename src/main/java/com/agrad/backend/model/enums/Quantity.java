package com.agrad.backend.model.enums;

public enum Quantity {

    KG("kg"),
    M3("m3"),
    T("t");

    Quantity(String value) {
    }
}
