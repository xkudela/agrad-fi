package com.agrad.backend.model.enums;

import lombok.Getter;

@Getter
public enum TransactionCategory {
    OFFER("Offer"),
    DEMAND("Demand");

    private final String name;

    TransactionCategory(String name) {
        this.name = name;
    }
}
