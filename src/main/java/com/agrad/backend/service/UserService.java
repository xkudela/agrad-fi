package com.agrad.backend.service;

import com.agrad.backend.model.Address;
import com.agrad.backend.model.User;
import com.agrad.backend.payload.requests.UserChangeRequest;
import com.agrad.backend.payload.response.AddressDetail;
import com.agrad.backend.payload.response.UserDetailResponse;
import com.agrad.backend.repository.AddressRepository;
import com.agrad.backend.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final AddressRepository addressRepository;


    public UserService(UserRepository userRepository, AddressRepository addressRepository) {
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
    }


    public UserDetailResponse getUserResponse(Long id) {
        Optional<User> userOpt = id == null ? userRepository.findByEmail(AuthService.getUserEmail())
                : userRepository.findById(id);
        if (userOpt.isEmpty()) {
            return null;
        }
        var user = userOpt.get();
        Address address = user.getAddress();
        AddressDetail addressDetail = address != null ? new AddressDetail(address.getName(), address.getLocation(), address.getRegion(), address.getLatitude(), address.getLongitude()) : null;
        var authorized = AuthService.isUserAuthenticated();

        return new UserDetailResponse(authorized ? user.getEmail() : null, user.getName(), authorized ? user.getPhoneNumber() : null,
                addressDetail);
    }

    public boolean updateUser(UserChangeRequest changeRequest) {
        Optional<User> existingUserOpt = userRepository.findByEmail(AuthService.getUserEmail());
        if (existingUserOpt.isEmpty()) {
            return false;
        }

        var existingUser = existingUserOpt.get();
        existingUser.setName(changeRequest.name());
        existingUser.setPhoneNumber(changeRequest.phoneNumber());

        if (changeRequest.address() != null) {
            Address address = existingUser.getAddress();
            if (address == null) {
                address = new Address();
                existingUser.setAddress(address);
            }
            address.setName(changeRequest.address().name());
            address.setLocation(changeRequest.address().location());
            address.setRegion(changeRequest.address().region());
            address.setLongitude(changeRequest.address().longitude());
            address.setLatitude(changeRequest.address().latitude());
            addressRepository.save(address);
        }

        userRepository.save(existingUser);
        return true;
    }

}