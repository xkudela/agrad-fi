package com.agrad.backend.service;

import com.agrad.backend.model.Address;
import com.agrad.backend.model.User;
import com.agrad.backend.payload.response.AddressDetail;
import com.agrad.backend.payload.response.Auth0UserInfo;
import com.agrad.backend.payload.response.UserDetailResponse;
import com.agrad.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * @author richard.kudela
 * @since 08.01.2024
 */
@Service
public class AuthService {

    @Value("${okta.oauth2.issuer}")
    private String authIssuer;

    private final UserRepository userRepository;

    public AuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDetailResponse registerUserLocally() {
        var userEmail = getUserEmail();
        var userOpt = userRepository.findByEmail(userEmail);
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            Address address = user.getAddress();
            AddressDetail addressDetail = address != null ? new AddressDetail(address.getName(), address.getLocation(), address.getRegion(), address.getLatitude(), address.getLongitude()) : null;
            return new UserDetailResponse(userEmail, user.getName(), user.getPhoneNumber(), addressDetail);
        }
        var userInfo = getUserInfo();
        var user = User.builder().email(userInfo.email()).name(userInfo.name()).build();
        userRepository.save(user);
        return new UserDetailResponse(userInfo.email(), userInfo.name(), null, null);
    }

    public static String getUserEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            throw new RuntimeException("Not known email!");
        }

        Object principal = authentication.getPrincipal();
        if (principal instanceof Jwt jwt) {
            Map<String, Object> claims = jwt.getClaims();
            return (String) claims.get("email");
        }

        return null;
    }

    public static boolean isUserAuthenticated() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && authentication.isAuthenticated() && !isAnonymous(authentication);
    }

    private static boolean isAnonymous(Authentication authentication) {
        return authentication instanceof AnonymousAuthenticationToken;
    }

    private Auth0UserInfo getUserInfo() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String jwtToken = ((JwtAuthenticationToken) authentication).getToken().getTokenValue();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", "Bearer " + jwtToken);

        HttpEntity<String> entity = new HttpEntity<>(headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Auth0UserInfo> response = restTemplate.exchange(authIssuer + "userinfo", HttpMethod.GET, entity, Auth0UserInfo.class);
        return response.getBody();
    }
}
