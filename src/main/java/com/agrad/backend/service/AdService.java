package com.agrad.backend.service;

import com.agrad.backend.model.AdData;
import com.agrad.backend.model.Address;
import com.agrad.backend.model.Advertisement;
import com.agrad.backend.model.AdvertisementCategory;
import com.agrad.backend.model.AdvertisementType;
import com.agrad.backend.model.CategoryData;
import com.agrad.backend.model.FavoriteAdvertisement;
import com.agrad.backend.model.User;
import com.agrad.backend.payload.requests.AdPayload;
import com.agrad.backend.payload.requests.FilterRequest;
import com.agrad.backend.payload.response.AdResponse;
import com.agrad.backend.payload.response.AdSummaryPayload;
import com.agrad.backend.payload.response.AddressDetail;
import com.agrad.backend.payload.response.AddressLocation;
import com.agrad.backend.payload.response.FilterResponse;
import com.agrad.backend.payload.response.UserWithoutAddress;
import com.agrad.backend.repository.AdDataRepository;
import com.agrad.backend.repository.AddressRepository;
import com.agrad.backend.repository.AdvertisementCategoryRepository;
import com.agrad.backend.repository.AdvertisementRepository;
import com.agrad.backend.repository.AdvertisementTypeRepository;
import com.agrad.backend.repository.FavoriteAdvertisementRepository;
import com.agrad.backend.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.agrad.backend.service.FileUtils.createAdDirectory;
import static com.agrad.backend.service.FileUtils.getFirstImageForAdvertisement;
import static com.agrad.backend.service.FileUtils.getImagesForAdvertisement;
import static com.agrad.backend.service.FileUtils.saveFile;

@Service
public class AdService {

    private final AddressService addressService;
    private final UserRepository userRepository;
    private final AddressRepository addressRepository;
    private final AdvertisementRepository advertisementRepository;
    private final AdvertisementCategoryRepository advertisementCategoryRepository;
    private final AdDataRepository adDataRepository;
    private final AdvertisementTypeRepository advertisementTypeRepository;
    private final FavoriteAdvertisementRepository favoriteAdvertisementRepository;

    public AdService(AddressService addressService, UserRepository userRepository, AddressRepository addressRepository, AdvertisementRepository advertisementRepository, AdvertisementCategoryRepository advertisementCategoryRepository, AdDataRepository adDataRepository, AdvertisementTypeRepository advertisementTypeRepository, FavoriteAdvertisementRepository favoriteAdvertisementRepository) {
        this.addressService = addressService;
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
        this.advertisementRepository = advertisementRepository;
        this.advertisementCategoryRepository = advertisementCategoryRepository;
        this.adDataRepository = adDataRepository;
        this.advertisementTypeRepository = advertisementTypeRepository;
        this.favoriteAdvertisementRepository = favoriteAdvertisementRepository;
    }


    public boolean createAd(AdPayload ad) {
        var advertisementOpt = advertisementRepository.findById(ad.adId());
        if (advertisementOpt.isEmpty()) {
            return false;
        }

        AddressDetail addressDetail = ad.address();
        Address address = new Address(addressDetail.name(), addressDetail.location(), addressDetail.region(), addressDetail.longitude(), addressDetail.latitude());
        var advertisement = advertisementOpt.get();
        var userOpt = userRepository.findByEmail(AuthService.getUserEmail());
        if (userOpt.isEmpty()) {
            advertisementRepository.delete(advertisement);
            return false;
        }

        var user = userOpt.get();
        addressRepository.save(address);

        advertisement.setAddress(address);
        advertisement.setUser(user);
        advertisement.setTitle(ad.title());
        advertisement.setTransactionType(ad.transactionCategory());
        advertisement.setPrice(ad.price());
        advertisement.setDescription(ad.description());
        advertisement.setViewCount(0);
        advertisement.setCreationDate(LocalDateTime.now());

        var category = advertisementCategoryRepository.findByName(ad.category());
        if (category == null) {
            return false;
        }

        advertisement.setCategory(category);

        for (CategoryData parameter : category.getCategoryDataList()) {
            String value = ad.additionalFields().get(parameter.getParameterName());
            if (value == null) {
                continue;
            }
            AdData adData = new AdData();
            adData.setAdvertisement(advertisement);
            adData.setCategoryData(parameter);
            adData.setValue(value);
            adDataRepository.save(adData);
        }

        advertisementRepository.save(advertisement);
        return true;
    }

    public FilterResponse getFiltersValues() {
        return new FilterResponse(advertisementRepository.findMinimumPrice(), advertisementRepository.findMaximumPrice(),
                addressService.findAllRegionsWithAdvertisementCountMap());
    }

    public AdResponse getAdvertisement(Long adId, boolean authenticated) {
        var adOpt = advertisementRepository.findById(adId);
        if (adOpt.isEmpty()) {
            return null;
        }

        var ad = adOpt.get();
        var completeUser = ad.getUser();
        var user = authenticated ? new UserWithoutAddress(completeUser.getId(), completeUser.getName(), completeUser.getPhoneNumber(), completeUser.getEmail())
                : new UserWithoutAddress(completeUser.getId(), completeUser.getName(), null, null);
        var images = getImagesForAdvertisement(ad);
        var additionalFields = new HashMap<String, String>();

        for (CategoryData categoryData : ad.getCategory().getCategoryDataList()) {
            String parameterName = categoryData.getParameterName();
            var adData = adDataRepository.findByAdvertisement_IdAndCategoryData(adId, categoryData);
            if (adData != null && adData.getValue() != null) {
                additionalFields.put(parameterName, adData.getValue());
            }
        }

        var address = ad.getAddress();
        var addressDetail = new AddressDetail(address.getName(), address.getLocation(), address.getRegion(), address.getLatitude(), address.getLongitude());

        boolean isFavorite = false;
        if (authenticated) {
            var browsingUser = userRepository.findByEmail(AuthService.getUserEmail());
            isFavorite = browsingUser.isPresent() && favoriteAdvertisementRepository.findByAdvertisementAndUser(ad, browsingUser.get()).isPresent();
        }

        return new AdResponse(ad.getCategory().getAdvertisementType().getName(), ad.getCategory().getName(), ad.getTitle(), ad.getDescription(), ad.getPrice(), ad.getTransactionType().getName(),
                user, addressDetail, images, additionalFields, isFavorite);
    }

    public List<AdSummaryPayload> getAllAdvertisements(FilterRequest filterRequest) {
        AddressDetail addressDetail = filterRequest.address();
        List<Advertisement> advertisements = advertisementRepository.findByCustomCriteria(
                filterRequest.keyword(),
                filterRequest.adType(),
                filterRequest.category(),
                filterRequest.minPrice(),
                filterRequest.maxPrice(),
                filterRequest.offer(),
                filterRequest.demand(),
                filterRequest.regions() == null || filterRequest.regions().length == 0 ? null : filterRequest.regions(),
                addressDetail != null ? addressDetail.latitude() : 0D,
                addressDetail != null ? addressDetail.longitude() : 0D,
                filterRequest.radius()
        );

        return getAdSummaryPayloads(advertisements);
    }


    public List<AdSummaryPayload> getAllUserAdvertisements(Long id) {
        var existingUserOpt = id == null ? userRepository.findByEmail(AuthService.getUserEmail())
                : userRepository.findById(id);
        if (existingUserOpt.isEmpty()) {
            return null;
        }

        var ads = advertisementRepository.findByUser(existingUserOpt.get());
        return getAdSummaryPayloads(ads);
    }

    public Long uploadFiles(MultipartFile[] files) {
        boolean allImages = Arrays.stream(files).allMatch(file -> {
            String contentType = file.getContentType();
            return contentType != null && contentType.startsWith("image");
        });

        if (!allImages) {
            return -1L;
        }

        var ad = new Advertisement();
        advertisementRepository.save(ad);
        var adId = ad.getId();

        Path adDir;
        try {
            adDir = createAdDirectory(adId);

            for (MultipartFile file : files) {
                saveFile(file, adDir);
            }

        } catch (IOException e) {
            advertisementRepository.delete(ad);
            throw new RuntimeException(e);
        }

        ad.setImagePath(adDir.getFileName().toString());
        advertisementRepository.save(ad);
        return adId;
    }

    public boolean increaseViewCount(Long adId) {
        var adOpt = advertisementRepository.findById(adId);
        if (adOpt.isEmpty()) {
            return false;
        }
        var ad = adOpt.get();
        var actualViewCount = ad.getViewCount();
        ad.setViewCount(++actualViewCount);
        advertisementRepository.save(ad);
        return true;
    }


    public HttpStatus deleteAd(Long adId) {
        var adOpt = advertisementRepository.findById(adId);
        if (adOpt.isEmpty()) {
            return HttpStatus.NOT_FOUND;
        }
        Advertisement ad = adOpt.get();
        if (!ad.getUser().getEmail().equals(AuthService.getUserEmail())) {
            return HttpStatus.UNAUTHORIZED;
        }
        advertisementRepository.delete(ad);
        return HttpStatus.OK;
    }

    public HttpStatus favoriteAd(Long adId) {
        var adOpt = advertisementRepository.findById(adId);
        if (adOpt.isEmpty()) {
            return HttpStatus.NOT_FOUND;
        }

        var userOpt = userRepository.findByEmail(AuthService.getUserEmail());
        if (userOpt.isEmpty()) {
            return HttpStatus.UNAUTHORIZED;
        }

        Advertisement advertisement = adOpt.get();
        User user = userOpt.get();
        var fav = favoriteAdvertisementRepository.findByAdvertisementAndUser(advertisement, user);
        if (fav.isEmpty()) {
            FavoriteAdvertisement favoriteAdvertisement = new FavoriteAdvertisement();
            favoriteAdvertisement.setAdvertisement(advertisement);
            favoriteAdvertisement.setUser(user);
            favoriteAdvertisementRepository.save(favoriteAdvertisement);
        } else {
            favoriteAdvertisementRepository.delete(fav.get());
        }
        return HttpStatus.OK;

    }

    public Map<String, List<String>> getAllCategories() {
        return advertisementTypeRepository.findAll().stream()
                .collect(Collectors.toMap(
                        AdvertisementType::getName,
                        this::getCategoryNamesForType
                ));
    }

    public List<AdSummaryPayload> getFavoriteAds() {
        var ads = favoriteAdvertisementRepository.findFavoriteAdvertisementByUserEmail(AuthService.getUserEmail())
                .stream().map(FavoriteAdvertisement::getAdvertisement).toList();
        return getAdSummaryPayloads(ads);
    }

    private List<String> getCategoryNamesForType(AdvertisementType type) {
        return type.getCategories().stream()
                .map(AdvertisementCategory::getName)
                .collect(Collectors.toList());
    }

    public List<String> getCategoryData(String category) {
        return advertisementCategoryRepository.findByName(category)
                .getCategoryDataList().stream()
                .map(CategoryData::getParameterName)
                .collect(Collectors.toList());
    }

    private List<AdSummaryPayload> getAdSummaryPayloads(List<Advertisement> ads) {
        return ads.stream()
                .map(ad -> {
                    String images = getFirstImageForAdvertisement(ad);
                    Address fullAddress = ad.getAddress();
                    var address = new AddressLocation(fullAddress.getName(), fullAddress.getLocation(), fullAddress.getRegion());
                    return new AdSummaryPayload(
                            ad.getId(),
                            ad.getTitle(),
                            ad.getCategory().getAdvertisementType().getName(),
                            ad.getCategory().getName(),
                            ad.getPrice(),
                            ad.getCreationDate(),
                            ad.getViewCount(),
                            address,
                            images
                    );
                })
                .toList();
    }

}
