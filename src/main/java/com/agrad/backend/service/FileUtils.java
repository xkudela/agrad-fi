package com.agrad.backend.service;

import com.agrad.backend.model.Advertisement;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@Service
public abstract class FileUtils {

    private FileUtils() {
    }

    private static final String UPLOAD_DIR = "/var/www/build/images";

    public static void deleteAdDirectory(Long adId) {
        try {
            Path adDir = Path.of(UPLOAD_DIR, "ad-" + adId.toString());

            Files.walkFileTree(adDir, EnumSet.noneOf(FileVisitOption.class), Integer.MAX_VALUE, new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) {
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new RuntimeException("Failed to delete ad directory", e);
        }
    }

    public static String getFirstImageForAdvertisement(Advertisement advertisement) {
        String imagePath = advertisement.getImagePath();

        if (imagePath != null && !imagePath.isEmpty()) {
            File imageFolder = new File(UPLOAD_DIR + File.separator + imagePath);

            if (imageFolder.isDirectory()) {
                File[] files = imageFolder.listFiles();

                if (files != null && files.length > 0) {
                    File firstImage = Arrays.stream(files).min(Comparator.comparing(File::getName))
                            .orElse(null);

                    return firstImage != null ? "images" + File.separator + imagePath + File.separator + firstImage.getName() : null;
                }
            }
        }

        return null;
    }

    public static List<String> getImagesForAdvertisement(Advertisement advertisement) {
        String imagePath = advertisement.getImagePath();

        if (imagePath != null && !imagePath.isEmpty()) {
            File imageFolder = new File(UPLOAD_DIR + File.separator + imagePath);

            if (imageFolder.isDirectory()) {
                File[] files = imageFolder.listFiles();

                if (files != null) {
                    return Stream.of(files)
                            .map(file -> "images" + File.separator + imagePath + File.separator + file.getName())
                            .toList();
                }
            }
        }

        return Collections.emptyList();
    }

    public static Path createAdDirectory(Long adId) throws IOException {
        Path adDir = Path.of(UPLOAD_DIR, "ad-" + adId.toString());

        Files.createDirectories(adDir);
        return adDir;
    }

    public static void saveFile(MultipartFile file, Path targetDirectory) throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());

        String originalFileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String fileExtension = StringUtils.getFilenameExtension(originalFileName);
        String uniqueFileName = "image_" + timestamp + "." + fileExtension;

        Path filePath = targetDirectory.resolve(uniqueFileName).toAbsolutePath().normalize();
        Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
    }


}
