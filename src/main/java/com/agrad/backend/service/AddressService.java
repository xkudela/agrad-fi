package com.agrad.backend.service;

import com.agrad.backend.repository.AddressRepository;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AddressService {
    private final AddressRepository addressRepository;

    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public List<Map<String, Long>> findAllRegionsWithAdvertisementCountMap() {
        List<Object[]> result = addressRepository.findAllRegionsWithAdvertisementCount();

        List<Object[]> filteredResult = result.stream()
                .filter(arr -> arr[0] != null)
                .toList();

        return filteredResult.stream()
                .map(arr -> {
                    Map<String, Long> map = new HashMap<>();
                    map.put((String) arr[0], (Long) arr[1]);
                    return map;
                })
                .toList();
    }
}
