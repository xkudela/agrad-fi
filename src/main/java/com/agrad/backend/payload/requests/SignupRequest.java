package com.agrad.backend.payload.requests;

/**
 * @author richard.kudela
 * @since 08.01.2024
 */
public record SignupRequest(String name, String email, String password) {

}
