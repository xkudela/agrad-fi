package com.agrad.backend.payload.requests;

import com.agrad.backend.payload.response.AddressDetail;

public record UserChangeRequest(String name, String phoneNumber, AddressDetail address) {
}
