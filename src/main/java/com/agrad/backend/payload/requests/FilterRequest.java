package com.agrad.backend.payload.requests;

import com.agrad.backend.payload.response.AddressDetail;

public record FilterRequest(String keyword, String adType, String category, Double minPrice, Double maxPrice,
                            Boolean offer, Boolean demand, String[] regions, AddressDetail address,
                            Integer radius) {
}
