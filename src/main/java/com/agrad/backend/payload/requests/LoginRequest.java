package com.agrad.backend.payload.requests;

public record LoginRequest(String email, String password) {
}
