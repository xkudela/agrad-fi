package com.agrad.backend.payload.requests;

import com.agrad.backend.model.enums.TransactionCategory;
import com.agrad.backend.payload.response.AddressDetail;

import java.util.Map;

public record AdPayload(
        Long adId,
        String title,
        String description,
        Double price,
        TransactionCategory transactionCategory,
        AddressDetail address,
        String category,
        Map<String, String> additionalFields
) {
}
