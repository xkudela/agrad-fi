package com.agrad.backend.payload.response;

import java.time.LocalDateTime;

public record AdSummaryPayload(
        Long id,
        String title,
        String adType,
        String category,
        Double price,
        LocalDateTime creationDate,
        int viewCount,
        AddressLocation address,
        String imagePath
) {
}
