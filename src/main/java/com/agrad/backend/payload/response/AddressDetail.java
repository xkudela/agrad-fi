package com.agrad.backend.payload.response;

public record AddressDetail(String name, String location, String region, Double latitude, Double longitude) {
}
