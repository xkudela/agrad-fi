package com.agrad.backend.payload.response;

public record UserDetailResponse(String email, String name, String phoneNumber, AddressDetail address) {
}
