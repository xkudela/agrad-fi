package com.agrad.backend.payload.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author richard.kudela
 * @since 08.01.2024
 */
@AllArgsConstructor
public class MessageResponse {

    @Getter
    @Setter
    private String message;

}
