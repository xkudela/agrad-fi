package com.agrad.backend.payload.response;

public record UserWithoutAddress(Long id, String name, String phoneNumber, String email) {
}
