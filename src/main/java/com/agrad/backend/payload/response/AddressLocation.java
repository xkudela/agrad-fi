package com.agrad.backend.payload.response;

public record AddressLocation(String name, String location, String region) {
}
