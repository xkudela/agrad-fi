package com.agrad.backend.payload.response;

import java.util.List;

public record OtherUserInfoResponse(UserDetailResponse user, List<AdSummaryPayload> ads) {
}