package com.agrad.backend.payload.response;

public record Auth0UserInfo(String email, String name) {
}
