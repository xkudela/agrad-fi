package com.agrad.backend.payload.response;

import java.util.List;
import java.util.Map;

public record FilterResponse(Double minPrice, Double maxPrice, List<Map<String, Long>> regions) {
}
