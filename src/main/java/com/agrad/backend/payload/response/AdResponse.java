package com.agrad.backend.payload.response;

import java.util.List;
import java.util.Map;

public record AdResponse(
        String adType,
        String category,
        String name,
        String description,
        Double price,
        String transactionCategory,
        UserWithoutAddress user,
        AddressDetail address,
        List<String> images,
        Map<String, String> additionalFields,
        Boolean isFavorite
) {
}
