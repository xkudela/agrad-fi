package com.agrad.backend.payload.response;

public record JwtResponse(String accessToken, Long id, String username) {

}