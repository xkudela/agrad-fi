package com.agrad.backend.repository;

import com.agrad.backend.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AddressRepository extends JpaRepository<Address, Long> {

    @Query("SELECT a.region, COUNT(a.region) FROM Advertisement ad JOIN ad.address a WHERE a.region IS NOT NULL GROUP BY a.region")
    List<Object[]> findAllRegionsWithAdvertisementCount();
}
