package com.agrad.backend.repository;

import com.agrad.backend.model.Advertisement;
import com.agrad.backend.model.User;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface AdvertisementRepository extends JpaRepository<Advertisement, Long> {

    @Transactional
    List<Advertisement> findByUser(User user);

    @Query("SELECT MIN(a.price) FROM Advertisement a")
    Double findMinimumPrice();

    @Query("SELECT MAX(a.price) FROM Advertisement a")
    Double findMaximumPrice();

    /**
     * @param keyword
     * @param adType
     * @param category
     * @param minPrice
     * @param maxPrice
     * @param offer
     * @param demand
     * @param regions
     * @param latitude
     * @param longitude
     * @param radius    - in kilometres
     * @return List of advertisements by filter
     */
    @Transactional
    @Query("SELECT a FROM Advertisement a WHERE " +
            "(COALESCE(:keyword, '') = '' OR a.title LIKE CONCAT('%', :keyword, '%')) AND " +
            "(COALESCE(:category, '') = '' OR a.category.name LIKE :category) AND " +
            "(COALESCE(:adType, '') = '' OR a.category.advertisementType.name LIKE :adType) AND " +
            "(:minPrice IS NULL OR a.price >= :minPrice) AND " +
            "(:maxPrice IS NULL OR a.price <= :maxPrice) AND " +
            "(:offer IS NULL OR :offer = false OR (:offer = true AND a.transactionType = 'OFFER')) AND " +
            "(:demand IS NULL OR :demand = false OR (:demand = true AND a.transactionType = 'DEMAND')) AND " +
            "(:regions IS NULL OR a.address.region IN :regions) AND " +
            "(:latitude = 0.0 OR :longitude = 0.0 OR :radius IS NULL OR " +
            "ST_Distance(ST_Transform(a.address.coordinates, 2163)," +
            "           ST_Transform(ST_SetSRID(ST_MakePoint(:longitude, :latitude), 4326), 2163)) <= :radius * 1000)" +
            "ORDER BY a.creationDate DESC")
    List<Advertisement> findByCustomCriteria(
            @Param("keyword") String keyword,
            @Param("adType") String adType,
            @Param("category") String category,
            @Param("minPrice") Double minPrice,
            @Param("maxPrice") Double maxPrice,
            @Param("offer") Boolean offer,
            @Param("demand") Boolean demand,
            @Param("regions") String[] regions,
            @Param("latitude") Double latitude,
            @Param("longitude") Double longitude,
            @Param("radius") Integer radius
    );

}
