package com.agrad.backend.repository;

import com.agrad.backend.model.Advertisement;
import com.agrad.backend.model.FavoriteAdvertisement;
import com.agrad.backend.model.User;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FavoriteAdvertisementRepository extends JpaRepository<FavoriteAdvertisement, Long> {

    @Transactional
    List<FavoriteAdvertisement> findFavoriteAdvertisementByUserEmail(String email);

    Optional<FavoriteAdvertisement> findByAdvertisementAndUser(Advertisement advertisement, User user);
}
