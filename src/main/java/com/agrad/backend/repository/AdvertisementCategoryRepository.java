package com.agrad.backend.repository;

import com.agrad.backend.model.AdvertisementCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdvertisementCategoryRepository extends JpaRepository<AdvertisementCategory, Long> {

    AdvertisementCategory findByName(String name);
}
