package com.agrad.backend.repository;

import com.agrad.backend.model.AdvertisementType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdvertisementTypeRepository extends JpaRepository<AdvertisementType, Long> {
}
