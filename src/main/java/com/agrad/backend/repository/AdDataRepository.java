package com.agrad.backend.repository;

import com.agrad.backend.model.AdData;
import com.agrad.backend.model.CategoryData;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdDataRepository extends JpaRepository<AdData, Long> {
    @Transactional
    AdData findByAdvertisement_IdAndCategoryData(Long adId, CategoryData categoryData);
}
