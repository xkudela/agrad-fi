package com.agrad.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author richard.kudela
 * @since 25.11.2023
 */
@SpringBootApplication
public class AgradApplication {

    public static void main(String[] args) {
        SpringApplication.run(AgradApplication.class, args);
    }

}
