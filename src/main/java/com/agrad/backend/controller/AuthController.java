package com.agrad.backend.controller;


import com.agrad.backend.payload.response.UserDetailResponse;
import com.agrad.backend.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/register")
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping
    public ResponseEntity<UserDetailResponse> registerUserLocally() {
        return ResponseEntity.ok(authService.registerUserLocally());
    }
}
