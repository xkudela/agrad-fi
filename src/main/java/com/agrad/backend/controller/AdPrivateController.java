package com.agrad.backend.controller;

import com.agrad.backend.payload.requests.AdPayload;
import com.agrad.backend.payload.response.AdSummaryPayload;
import com.agrad.backend.service.AdService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static com.agrad.backend.service.FileUtils.deleteAdDirectory;

@RestController
@RequestMapping("/api/adAuth")
public class AdPrivateController {

    private final AdService adService;

    public AdPrivateController(AdService adService) {
        this.adService = adService;
    }

    @PostMapping
    public ResponseEntity<Long> createAd(@RequestBody AdPayload ad) {
        var success = adService.createAd(ad);
        if (!success) {
            deleteAdDirectory(ad.adId());
        }
        return success ? ResponseEntity.ok(ad.adId()) : ResponseEntity.unprocessableEntity().build();
    }

    @PostMapping(value = "/upload", consumes = {"multipart/form-data", "image/jpeg", "image/png"})
    public ResponseEntity<Long> handleFileUpload(@RequestParam("files") MultipartFile[] files) {
        if (files == null || files.length == 0 || files.length > 10) {
            return ResponseEntity.badRequest().build();
        }

        Long adId = adService.uploadFiles(files);
        return adId != -1 ? ResponseEntity.ok(adId) : ResponseEntity.badRequest().build();
    }

    @GetMapping("/userAds")
    public ResponseEntity<List<AdSummaryPayload>> getAllUserAdvertisements() {
        var ads = adService.getAllUserAdvertisements(null);
        return ads == null || ads.isEmpty() ? new ResponseEntity<>(HttpStatus.NOT_FOUND) : ResponseEntity.ok(ads);
    }

    @GetMapping("/{adId}")
    public ResponseEntity<Void> favoriteAd(@PathVariable Long adId) {
        return new ResponseEntity<>(adService.favoriteAd(adId));
    }

    @DeleteMapping("/{adId}")
    public ResponseEntity<Void> deleteAd(@PathVariable Long adId) {
        return new ResponseEntity<>(adService.deleteAd(adId));
    }

    @GetMapping("/favoriteAds")
    public ResponseEntity<List<AdSummaryPayload>> getFavoriteAds() {
        return ResponseEntity.ok(adService.getFavoriteAds());
    }
}
