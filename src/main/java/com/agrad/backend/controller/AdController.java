package com.agrad.backend.controller;

import com.agrad.backend.payload.requests.FilterRequest;
import com.agrad.backend.payload.response.AdResponse;
import com.agrad.backend.payload.response.AdSummaryPayload;
import com.agrad.backend.payload.response.FilterResponse;
import com.agrad.backend.service.AdService;
import com.agrad.backend.service.AuthService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/ad")
public class AdController {

    private final AdService adService;

    public AdController(AdService adService) {
        this.adService = adService;
    }

    @PostMapping("/all")
    public ResponseEntity<List<AdSummaryPayload>> getAds(@RequestBody FilterRequest filterRequest) {
        return ResponseEntity.ok(adService.getAllAdvertisements(filterRequest));
    }

    @GetMapping("/{adId}")
    public ResponseEntity<AdResponse> getAd(@PathVariable Long adId) {
        var authenticated = AuthService.isUserAuthenticated();
        return ResponseEntity.ok(adService.getAdvertisement(adId, authenticated));
    }

    @GetMapping("/categories")
    public ResponseEntity<Map<String, List<String>>> getCategories() {
        return ResponseEntity.ok(adService.getAllCategories());
    }

    @GetMapping("/categories/{category}")
    public ResponseEntity<List<String>> getCategoryData(@PathVariable String category) {
        return ResponseEntity.ok(adService.getCategoryData(category));
    }

    @GetMapping("/filters")
    public ResponseEntity<FilterResponse> getFiltersValues() {
        return ResponseEntity.ok(adService.getFiltersValues());
    }

    @GetMapping("/increaseViews/{adId}")
    public ResponseEntity<Void> increaseViewCount(@PathVariable Long adId) {
        return new ResponseEntity<>(adService.increaseViewCount(adId) ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
}
