package com.agrad.backend.controller;

import com.agrad.backend.payload.requests.UserChangeRequest;
import com.agrad.backend.payload.response.OtherUserInfoResponse;
import com.agrad.backend.payload.response.UserDetailResponse;
import com.agrad.backend.service.AdService;
import com.agrad.backend.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserService userService;
    private final AdService adService;

    public UserController(UserService userService, AdService adService) {
        this.userService = userService;
        this.adService = adService;
    }

    @GetMapping("/profile")
    public ResponseEntity<UserDetailResponse> getUser() {
        UserDetailResponse userResponse = userService.getUserResponse(null);
        return userResponse != null ? ResponseEntity.ok(userResponse) : ResponseEntity.notFound().build();
    }

    @PutMapping("/updateProfile")
    public ResponseEntity<Void> updateUser(@RequestBody UserChangeRequest changeRequest) {
        return userService.updateUser(changeRequest) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }

    @GetMapping("/{userId}")
    public ResponseEntity<OtherUserInfoResponse> getUser(@PathVariable Long userId) {
        var userDetailResponse = userService.getUserResponse(userId);
        if (userDetailResponse == null) {
            return ResponseEntity.notFound().build();
        }
        var ads = adService.getAllUserAdvertisements(userId);
        return ResponseEntity.ok(new OtherUserInfoResponse(userDetailResponse, ads));
    }
}